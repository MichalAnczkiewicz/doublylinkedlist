#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class student {
	public:
		int index;
	    string name;
	    string surname;
	    int yearOfStudy;
	    string fieldOfStudy;
	    string specialty;
	    student *next; //wskaznik na nastepny
	    student *previous; //wskaznik na poprzedni
	    student();
};

student::student() {
    next = 0;
}

class llist {
	public:
	    student *first; //wskaznik na pierwszy
	    student *last; //wskaznik na ostatni
	    void insertAtEnd (int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty);
	    void insertAtBeginning (int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty);
	    void insertAtGivenPosition(int a, string n, int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty);
	    void findStudent(int index, string surname);
	    void deleteSelected(int index, string surname);
	    void deleteAll ();
	    void displayList ();
	    llist();
};

llist::llist() { //konstruktory
    first = 0;
}

void llist::insertAtBeginning(int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty){
	student *newStudent = new student; //tworzenie nowego elementu
	newStudent->index = index;
	newStudent->name = name;
	newStudent->surname = surname;
	newStudent->yearOfStudy = yearOfStudy;
	newStudent->fieldOfStudy = fieldOfStudy;
	newStudent->specialty = specialty;

	if (first==0){ // jesli nowy element bedzie pierwszym na liscie
		first = newStudent;
		last = newStudent;
		newStudent->next = 0; // bo nie ma nowych elementow
		newStudent->previous = 0; //bo nie ma poprzednich elementow
	}

	else { //jesli nie pierwszy
		newStudent->next = first; // nastepnikiem nowego elementu stanie sie obecny pierwszy element listy
		newStudent->previous = 0; // bo nie bedzie poprzenich elementow
		first = newStudent; //pierwszym element bedzie newStudent
	}
}

void llist::insertAtEnd(int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty){
	student *newStudent = new student; //tworzenie nowego obiektu
	newStudent->index = index;
	newStudent->name = name;
	newStudent->surname = surname;
	newStudent->yearOfStudy = yearOfStudy;
	newStudent->fieldOfStudy = fieldOfStudy;
	newStudent->specialty = specialty;

	if(first==0){ //wszystko analogicznie do dodawania na poczatek
		first = newStudent;
		last = newStudent;
		newStudent->next = 0;
		newStudent->previous = 0;
	}
	else { //jesli nie jest pierwszym
		newStudent->previous = last; // poprzednikiem nowego elementu stanie sie obecny ostatni element listy
		last->next = newStudent; // nastepnikiem obecnego ostatniego stanie sie newStudent
		last = newStudent; //newStudent ostatnim elementem
		newStudent->next = 0; //bo nie bedzie nastepnego
	}
}

void llist::insertAtGivenPosition(int a, string n, int index, string name, string surname, int yearOfStudy, string fieldOfStudy, string specialty){
	student *newStudent = new student; //tworzenie nowego elementu
	student *tmp = first; //tymczasowa wskaznik na pierwszy element
	newStudent->index = index;
	newStudent->name = name;
	newStudent->surname = surname;
	newStudent->yearOfStudy = yearOfStudy;
	newStudent->fieldOfStudy = fieldOfStudy;
	newStudent->specialty = specialty;
	while(tmp->index!=a || tmp->surname!=n){ //szukamy elementu kiedy index i nazwisko z funkcji beda sie zgadzac
		tmp=tmp->next;
	}
	if(tmp==last){ // jesli wstawiamy za ostatni, to instrukcje jak do wstawiania na ostatnie miejsce
		last->next=newStudent;
		newStudent->next=0; // bo nie ma nastepnego
		last=newStudent;
	}
	else{
		student *tmp2=tmp->next; //w tmp2 bedzie nastepnym po tmp
		tmp->next=newStudent; // next z tmp pokazuje teraz na newStudent
		newStudent->next=tmp2; //next z newStudent pokazuje teraz tmp2
	}
}

void llist::findStudent(int index, string surname){
	student *newStudent = new student; //tworzenie nowego obiektu
	student *tmp = first; //tymczasowa wskaznik wskazujaca na pierwszy element
	while(tmp){ // przeszukiwanie listy
		if(tmp->index == index && tmp->surname == surname){ // jesli argumenty z metody sa rowne danym z obiektu
			cout << "nr indexu: " << index << endl;
	        cout << "name: " << tmp->name << endl;
			cout << "surname: " << surname << endl;
			cout << "rok studiow: " << tmp->yearOfStudy << endl;
	        cout << "fieldOfStudy: " << tmp->fieldOfStudy << endl;
			cout << "specialty: " << tmp->specialty << endl << endl;
		}
		tmp=tmp->next; // dalej w liste
	}
}

void llist::deleteSelected(int index, string surname){
	student *tmp = first; //tymczasowy wskaznik na pierwszy
	student *previous = first; //tymczasowy
	while(tmp!=0){ // jesli nie ma konca listy
		if(tmp->index == index && tmp->surname == surname){ // jesli argumenty z metody sa rowne elementowi z listy
			if(tmp==first){ //jesli jest pierwszym
				first=first->next; // pierwszym elementem bedzie teraz element, ktory jest po pierwszym
				delete tmp; //usuwamy element
				tmp=first; //tmp jest nowym pierwszym
				previous=first; // przypisujemy pierwszy pod poprzedni
			}
			else if(tmp==last){ //jesli jest ostatnim elementem
				last=tmp->previous; // ostatnim elementem bedzie teraz element, ktory jest przedostatni
				last->next=0; //bo nie bedzie nastepnego elementu
				delete tmp; //usuwamy element
				tmp=last; // tmp jest ostatnim elementem
				previous=last; //przypisujemy ostatni pod poprzedni
			}
			else { //jesli w srodku
				previous->next = tmp->next; // nastepny z poprzedniego wskazuje na nastepny z tmp
				delete tmp; //usuwamy element
				tmp=previous->next; //tmp przechodzi na nastepny
			}
		}
		previous=tmp; //przypisanie tmp pod poprzedni
		tmp=tmp->next; //tmp przechodzi do nastepnego elementu listy
	}
}


void llist::deleteAll(){ //usuwanie wszystkich
	student *tmp = first; //tmp wskazuje na pierwszy

	while (tmp!=0){ //jesli nie pusta
		first = first->next; // nowym pierwszym jest pierwszy poprzedzajacy owczesny pierwszy
		delete tmp;
		tmp = first;
	}
	cout << "List was deleted." << endl;
}

void llist::displayList(){
	student *tmp = first; // tmp wskazuje na pierwszy
	if (first == 0) cout << "There are no students on the list." << endl;
	int i = 1;
	while (tmp) { //wskaznik na kolejne
		cout << "STUDENT NR " << i << ": " << endl;
		cout << "Index: " << tmp->index << endl;
		cout << "name: " << tmp->name << endl;
		cout << "surname: " << tmp->surname << endl;
		cout << "years of study: " << tmp->yearOfStudy << endl;
		cout << "field of study: " << tmp->fieldOfStudy << endl;
		cout << "specialty: " << tmp->specialty << endl << endl;
		tmp = tmp->next; //przechodzi do nastepnego elementu
		i++;
	}
}

void menu() {
	cout << "MENU" << endl;
	cout << "1. Insert element at the beginning.\n";
	cout << "2. Insert element at the end\n";
	cout << "3. Insert at the selected position\n";
	cout << "4. Display list\n";
	cout << "5. Find student\n";
	cout << "6. Delete selected element\n";
	cout << "7. Delete all elements\n";
	cout << "9. Display menu\n";
	cout << "0. End of program\n";
}


int main() {
	llist *students = new llist;
	int index, yearOfStudy, a;
	string name, surname, fieldOfStudy, specialty, n;
	menu();
	int choice;
	do
	{
		cout << "\nChoices: (9 - menu) ";
		cin >> choice;
		switch (choice)
		{
		case 1:
			cout << "Enter the student's index: ";
			cin >> index;
			cout << "Enter the student's name: ";
			cin >> name;
			cout << "Enter the student's surname: ";
			cin >> surname;
			cout << "Enter the student's years of study: ";
			cin >> yearOfStudy;
			cout << "Enter the student's field of study: ";
			cin >> fieldOfStudy;
			cout << "Enter the student's specialty: ";
			cin >> specialty;
			students->insertAtBeginning(index, name, surname, yearOfStudy, fieldOfStudy, specialty);
			break;
		case 2:
			cout << "Enter the student's index: ";
			cin >> index;
			cout << "Enter the student's name: ";
			cin >> name;
			cout << "Enter the student's surname: ";
			cin >> surname;
			cout << "Enter the student's years of study: ";
			cin >> yearOfStudy;
			cout << "Enter the student's field of study: ";
			cin >> fieldOfStudy;
			cout << "Enter the student's specialty: ";
			cin >> specialty;
			students->insertAtEnd(index, name, surname, yearOfStudy, fieldOfStudy, specialty);
			break;
		case 3:
			cout << "Enter the student's index number for which you want to insert another ";
			cin >> a;
			cout << "Enter his surname: ";
			cin >> n;
			cout << "Enter the student's index: ";
			cin >> index;
			cout <<"Enter the student's name; ";
			cin >> name;
			cout << "Enter the student's surname: ";
			cin >> surname;
			cout << "Enter the student's years of study: ";
			cin >> yearOfStudy;
			cout << "Enter the student's field of study: ";
			cin >> fieldOfStudy;
			cout << "Enter the student's specialty: ";
			cin >> specialty;
			students->insertAtGivenPosition(a, n, index, name, surname, yearOfStudy, fieldOfStudy, specialty);
			break;
		case 4:
			students->displayList();
			break;
		case 5:
			cout << "Enter the student's index that you are looking for: ";
			cin >> a;
			cout << "Enter his surname: ";
			cin >> n;
			cout << endl << "Info about the student that was found: " << endl;
			students->findStudent(a, n);
			break;
		case 6:
			cout << "Enter the student's index that you want to delete: ";
			cin >> a;
			cout << "Enter his surname: ";
			cin >> n;
			students->deleteSelected(a, n);
			cout << "Deleted: " << a << " " << n << endl;
			break;
		case 7:
			students->deleteAll();
			break;
		case 9:
			menu();
			break;
		case 0:
			cout << endl << "End of program." << endl;
			break;
		default:
			cout << "Wrong character, try one more time " << endl;
		}
	} while (choice != 0);

	delete(students);     //usuwamy liste

	return 0;
}
